package com.alura.strategy;

public class TestaImposto {

	public static void main (String [] args) {
		
		Imposto icms = new ICMS();
		Imposto iss = new ISS();
		Imposto iccc = new ICCC();
		
		Orcamento orcamento = new Orcamento(4000.0);
		
		CalculaImposto imposto = new CalculaImposto();
		imposto.calcular(orcamento, iss);
		imposto.calcular(orcamento, icms);
		imposto.calcular(orcamento, iccc);
		
	}
}
