package com.alura.strategy;

public interface Investimento {

	public double calcular(double valor);
	
}
