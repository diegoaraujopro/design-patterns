/**
 * 
 */
package com.alura.strategy;

/**
 * @author diego
 *
 */
public class Conservador implements Investimento{

	@Override
	public double calcular(double valor) {
		return valor * 0.008;
	}

}
