package com.alura.strategy;

public class CalculaImposto {

	public void calcular(Orcamento orcamento, Imposto impostoQualquer) {
		double valorImposto = impostoQualquer.calcular(orcamento);
		System.out.println(valorImposto);
	}
}
