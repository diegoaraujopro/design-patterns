/**
 * 
 */
package com.alura.strategy;

/**
 * @author diego
 *
 */
public class Arrojado implements Investimento {

	@Override
	public double calcular(double valor) {
		double retorno, chance = new java.util.Random().nextDouble() ;
		if (chance <= 0.5 )
			retorno = valor * 0.006;
		else if (chance <= 0.7 )
			retorno = valor * 0.03;
		else 
			retorno = valor * 0.05;
		return retorno;
	}

}
