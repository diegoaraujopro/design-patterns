/**
 * 
 */
package com.alura.strategy;

/**
 * @author diego
 *
 */
public class Moderado implements Investimento{

	@Override
	public double calcular(double valor) {
		double retorno = 0.0;
		if (new java.util.Random().nextDouble() >= 0.5) 
			retorno = valor *0.025;
		else 
			retorno = valor * 0.007;
		return retorno;
	}

}
