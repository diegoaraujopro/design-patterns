package com.alura.strategy;

public class TestaInvestimento {

	public static void main (String [] args) {
		
		double valor = 2000;
		
		Investimento conservador = new Conservador();
		Investimento moderado = new Moderado();
		Investimento arrojado = new Arrojado();
		
		CalcularInvestimento investir = new CalcularInvestimento();
		System.out.println("\nConservador:");
		investir.calcula(valor, conservador);
		investir.calcula(valor, conservador);
		investir.calcula(valor, conservador);
		
		System.out.println("\nModerado:");
		investir.calcula(valor, moderado);
		investir.calcula(valor, moderado);
		investir.calcula(valor, moderado);
		investir.calcula(valor, moderado);
		
		System.out.println("\nArrojado:");
		investir.calcula(valor, arrojado);
		investir.calcula(valor, arrojado);
		investir.calcula(valor, arrojado);
		investir.calcula(valor, arrojado);
		
		
	}
}
