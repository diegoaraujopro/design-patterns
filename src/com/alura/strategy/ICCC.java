package com.alura.strategy;

public class ICCC implements Imposto {

	@Override
	public double calcular(Orcamento orcamento) {
		double valor = orcamento.getValor();
		if (valor < 1000) {
			valor = valor * 0.05;
		} else if (valor <= 3000) {
			valor = valor * 0.07;
		} else {
			valor = valor * 0.08 + 30;
		}
		return valor;
	}

}
