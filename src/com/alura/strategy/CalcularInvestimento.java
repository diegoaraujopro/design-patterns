package com.alura.strategy;

public class CalcularInvestimento {
	
	public void calcula(double valor, Investimento investimentoQualquer) {
		double retorno = investimentoQualquer.calcular(valor);
		System.out.println(retorno);
	}
}
