package com.alura.chainOfResponsibility;

public interface Desconto {

	double calcular(Orcamento orcamento);

	void proximo(Desconto desconto);
}
