package com.alura.chainOfResponsibility;

enum Formato {
	XML, CSV, PORCENTO
}

public class Requisicao {

	private Formato formato;

	Requisicao(Formato formato) {
		this.formato = formato;
	}

	public Formato getFormato() {
		return formato;
	}
}
