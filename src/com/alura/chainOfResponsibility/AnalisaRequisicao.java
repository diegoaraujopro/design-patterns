package com.alura.chainOfResponsibility;

public class AnalisaRequisicao {

	public void ler(Requisicao req, Conta conta) {
		
		Resposta xml = new RespostaXML();
		Resposta csv = new RespostaCSV();
		Resposta por = new RespostaPORCENTO();
		
		csv.setProxima(por);
		xml.setProxima(csv);

		xml.responde(req, conta);
	}
}
