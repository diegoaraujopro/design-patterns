package com.alura.chainOfResponsibility;

public class RespostaXML implements Resposta {
	
	Resposta resposta;

	@Override
	public void responde(Requisicao req, Conta conta) {
		if (req.getFormato().equals(Formato.XML)) {
			StringBuilder res = new StringBuilder();
			res.append("<conta>");
			res.append("<titular>");
			res.append(conta.getTitular());
			res.append("</titular>");
			res.append("<saldo>");
			res.append(conta.getSaldo());
			res.append("</saldo>");
			res.append("</conta>");
			System.out.println(res.toString());
		} else {
			resposta.responde(req, conta);
		}
	}

	@Override
	public void setProxima(Resposta resposta) {
		this.resposta = resposta;
	}

}
