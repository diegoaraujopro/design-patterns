package com.alura.chainOfResponsibility;

public class Desconto10 implements Desconto {

	Desconto proximo;

	@Override
	public double calcular(Orcamento orcamento) {

		if (orcamento.getItens().size() > 5) {
			return orcamento.getValorTotal() * 0.1;
		} else {
			return proximo.calcular(orcamento);
		}
	}

	@Override
	public void proximo(Desconto desconto) {
		this.proximo = desconto;
	}

}
