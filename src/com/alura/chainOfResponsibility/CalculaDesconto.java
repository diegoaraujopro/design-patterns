package com.alura.chainOfResponsibility;

public class CalculaDesconto {

	public double calcular(Orcamento orcamento) {
		Desconto d1 = new Desconto10();
		Desconto d2 = new Desconto7();
		Desconto d999 = new Desconto0();
		
		d1.proximo(d2);
		d2.proximo(d999);
		
		return d1.calcular(orcamento);
	}
}
