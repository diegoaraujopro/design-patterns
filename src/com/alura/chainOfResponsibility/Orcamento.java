package com.alura.chainOfResponsibility;

import java.util.ArrayList;
import java.util.List;

public class Orcamento {

	private double valorTotal;
	private List<Item> itens = new ArrayList<>();

	public Orcamento(Item item) {
		this.itens.add(item);
		calcularTotal();
	}

	public double getValorTotal() {
		return valorTotal = calcularTotal();
	}

	public List<Item> getItens() {
		return itens;
	}

	private double calcularTotal() {
		valorTotal = 0;
		for (Item item : itens) {
			valorTotal += item.getValor();
		}
		return valorTotal;
	}
	
	public void adicionarItem(Item item) {
        itens.add(item);
        calcularTotal();
    }
}
