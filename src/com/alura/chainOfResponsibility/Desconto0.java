package com.alura.chainOfResponsibility;

public class Desconto0 implements Desconto {
	

	@Override
	public double calcular(Orcamento orcamento) {
		return 0;
	}

	@Override
	public void proximo(Desconto desconto) {
		// não existe próximo desconto, este é o último
	}

}
