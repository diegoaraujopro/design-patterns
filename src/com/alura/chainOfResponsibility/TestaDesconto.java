package com.alura.chainOfResponsibility;

import java.text.NumberFormat;
import java.util.Locale;

public class TestaDesconto {

	public static void main(String[] args) {

		Item i1 = new Item("Curso Algaworks", 697);
		Item i2 = new Item("Curso Alura", 54.90);
		Item i3 = new Item("Curso Udemy", 20);
		Item i4 = new Item("Curso Caelum", 350);
		Item i5 = new Item("Curso XPTO", 30.90);
		Item i6 = new Item("Curso XPTO2", 50.90);
		Item i7 = new Item("Curso XPTO3", 70.90);

		Orcamento o1 = new Orcamento(i1);
		
		CalculaDesconto cd1 = new CalculaDesconto();
		valorDesconto(o1.getValorTotal(), cd1.calcular(o1));
		
		Orcamento o2 = new Orcamento(i2);
		o2.adicionarItem(i3);
		o2.adicionarItem(i5);
		o2.adicionarItem(i6);
		o2.adicionarItem(i7);
		
		CalculaDesconto cd2 = new CalculaDesconto();
		valorDesconto(o2.getValorTotal(), cd2.calcular(o2));

	}

	static void valorDesconto(double total, double desconto) {
		NumberFormat real = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		System.out.println("Valor total: " + real.format(total) + " e tem desconto de: " + real.format(desconto));
	}
}
