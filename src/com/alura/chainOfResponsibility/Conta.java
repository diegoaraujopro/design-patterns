package com.alura.chainOfResponsibility;

public class Conta {

	String titular;
	double saldo;

	Conta(String titular, double saldo) {
		this.titular = titular;
		this.saldo = saldo;
	}

	public String getTitular() {
		return titular;
	}

	public double getSaldo() {
		return saldo;
	}

}
