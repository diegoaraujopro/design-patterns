package com.alura.chainOfResponsibility;

public class RespostaPORCENTO implements Resposta {

	Resposta resposta;

	@Override
	public void responde(Requisicao req, Conta conta) {
		if (req.getFormato().equals(Formato.PORCENTO)) {
			StringBuilder res = new StringBuilder();
			res.append(conta.getTitular());
			res.append("%");
			res.append(conta.getSaldo());
			System.out.println(res.toString());
		} else {
			resposta.responde(req, conta);
		}
	}

	@Override
	public void setProxima(Resposta resposta) {
		this.resposta = resposta;
	}

}
