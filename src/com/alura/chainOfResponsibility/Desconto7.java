package com.alura.chainOfResponsibility;

public class Desconto7 implements Desconto {
	
	Desconto proximo;

	@Override
	public double calcular(Orcamento orcamento) {
		if (orcamento.getValorTotal() > 500) {
			return orcamento.getValorTotal() * 0.07;
		} else {
			return proximo.calcular(orcamento);
		}
		
	}

	@Override
	public void proximo(Desconto desconto) {
		proximo = desconto;
	}

}
